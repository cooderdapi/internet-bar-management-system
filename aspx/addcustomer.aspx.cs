﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class aspx_addcustomer : System.Web.UI.Page
{
    // 声明数据源
    string url = @"Data Source=DESKTOP-FV9UNM8;Initial Catalog=db_internet_bar;Integrated Security=True";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            connection();

        }

    }

    private void connection()
    {
        
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(url);
            string sql = "select * from t_computer_status";
            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
            DataSet myds = new DataSet();
            conn.Open();
            myda.Fill(myds, "t_computer_status");
            GridView1.DataSource = myds;
            GridView1.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('异常')</script>");
        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }
        }
    }



    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        connection();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string id = ((TextBox)row.Cells[1].Controls[0]).Text;
        string s = ((TextBox)row.Cells[2].Controls[0]).Text;
        updateStatus(id, s);
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        connection();
    }
    
    private void updateStatus(string num, string s)
    {
        using (SqlConnection conn = new SqlConnection(url))
        {
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"update t_computer_status set status = @s where id = @num";
            cmd.Parameters.AddWithValue("@num", num);
            cmd.Parameters.AddWithValue("s", s);
            cmd.ExecuteNonQuery();
            cmd.CommandText = @"select * from t_computer_status";
            cmd.Parameters.Clear();
            SqlDataReader dr = cmd.ExecuteReader();
            GridView1.DataSource = dr;
            GridView1.EditIndex = -1;
            GridView1.DataBind();
            dr.Close();
            cmd.Dispose();
        }
    } 
}