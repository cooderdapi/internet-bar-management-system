﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aspx_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {
        //连接数据库
        string url = @"Data Source=DESKTOP-FV9UNM8;Initial Catalog=db_internet_bar;Integrated Security=True";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(url);
            conn.Open();
            string sql = "select * from t_user where username='{0}' and password='{1}'";
            sql = string.Format(sql, username.Text, password.Text);
            SqlCommand cmd = new SqlCommand(sql, conn);
            int returnValue = (int)cmd.ExecuteScalar();
            if (returnValue != 0)
            {
                //登录成功，进入系统
                Response.Redirect("../html/index.html");
            }
            else
            {
                Response.Redirect("../aspx/login.aspx");

            }
        }
        catch(Exception ex){
            Response.Write("<script>alert('登录异常')</script>");
        }
        finally
        {
            if(conn != null)
            {
                conn.Close();
            }
        }
    }
}