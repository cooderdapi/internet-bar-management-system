﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="customerAct.aspx.vb" Inherits="aspx_customerAct" %>

<!DOCTYPE html>
<link rel="Shortcut Icon" href="../img/icon.png" type="image/x-icon" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<link rel="stylesheet" href="../css/style.css" />
    <title>会员服务</title>
</head>
<body>
    <form id="form1" runat="server">
        <header>
          <nav>
            <ul>
              <li><a href="queryCustomer.aspx" target="right-nav">会员查询</a></li>
              <li><a href="register.aspx" target="right-nav">会员注册</a></li>
              <li><a href="#">会员活动</a></li>
              <li><a href="#">增值服务</a></li>
              <li><a href="#">积分兑换</a></li>
              <li><a href="#">杂项</a></li>
              <div class="slider"></div>
            </ul>
          </nav>
    </header>
    </form>
</body>
</html>
