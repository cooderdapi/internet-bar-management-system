﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class aspx_register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void submitBtn_Click(object sender, EventArgs e)
    {
        //连接数据库
        string url = @"Data Source=DESKTOP-FV9UNM8;Initial Catalog=db_internet_bar;Integrated Security=True";
        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(url);
            conn.Open();
            string sql = "insert into t_customer(name,idcard,balance) values('{0}','{1}','{2}')";
            sql = string.Format(sql, name.Text,idcard.Text,balance.Text);
            SqlCommand cmd = new SqlCommand(sql, conn);
            int returnValue = cmd.ExecuteNonQuery();
            if (returnValue != -1)
            {
                //注册成功

                Response.Redirect("../aspx/customerAct.aspx");
                Response.Write("<script>alert('注册成功')</script>");
            }
            else
            {
                //注册失败
                Response.Write("<script>alert('注册失败')</script>");

            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('注册异常')</script>");
        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }
        }
    }
}