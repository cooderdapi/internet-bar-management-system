﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //声明数据源
        string url = @"Data Source=DESKTOP-FV9UNM8;Initial Catalog=db_internet_bar;Integrated Security=True";

        SqlConnection conn = null;
        try
        {
            conn = new SqlConnection(url);
            string sql = "select * from t_computer_status";
            SqlDataAdapter myda = new SqlDataAdapter(sql, conn);
            DataSet myds = new DataSet();
            conn.Open();
            myda.Fill(myds, "t_computer_status");
            GridView1.DataSource = myds;
            GridView1.DataBind();

        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('异常')</script>");
        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }
        }
    }

    protected void butConn_Click(object sender, EventArgs e)
    {
        

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
}